# vudemo

#### 项目介绍
vue 前端练习，实现基本增删改查功能

#### 软件架构
vue webpack 


#### 新建vue +webpack空项目教程
1. node.js 安装教程https://www.runoob.com/nodejs/nodejs-install-setup.html
2. git-bash 命令行下载地址 https://git-for-windows.github.io/  安装的时候直接全部默认即可!
3. 安装vue-cil 命令： npm install -g vue-ci          
4. 进入到工程目录下命令：cd  vue-project(目录写自己的目录)    
5. 打开自己新建项目的目录：vue init webpack vuedemo    
6. 跑起项目来检测下： npm run dev    
#### 遇到的坑：记得找到build目录下的webpack.base.conf.js文件，注释检测代码的插件，此模块会校验我们的js代码格式，导致写的不严谨的也报错。下面是注释部分,这部分的注释坑以后会解决的，前期先熟悉为主，熟悉时间的基础上我们讨论真知
 ```
//const createLintingRule = () => ({      
//test: /\.(js|vue)$/,   
//loader: 'eslint-loader',  
//enforce: 'pre',  
//include: [resolve('src'), resolve('test')],  
//options: {  
//  formatter: require('eslint-friendly-formatter'),  
//  emitWarning: !config.dev.showEslintErrorsInOverlay  
//}  
//})  
//module所在的地方  
//   ...(config.dev.useEslint ? [createLintingRule()] : []),  
 ```


#### 使用说明
1. 打开从git上克隆下来的项目，git-bash打开到当前目录
2. 输入npm install 下载依赖,如果没有node和git bash 请下载，上面已给出链接
2. 输入npm  run build 是打包项目
3. 输入npm  run dev 是跑起我们的项目，
4. 浏览器中输入[http://localhost:8089/](http://localhost:8089/)

#### 目前涉及到的知识点
1. #### vue知识点
    | 序号 | 知识点                                |
    |-----|----------------------------------------|
    |  1  |  单页面程序路由配置                      |
    |  2  |  vue基础写法                            |
    |  3  |  统一api配置                            |
    |  4  |  vue-element-UI                        |
    |  5  |  过滤器配置                             |

2. #### webpack知识点
   webapck配置，具体配置需要参考官网

