import Vue from 'vue'
import Router from 'vue-router'
// 引用模板
import index from '../pages/index.vue'
import content from '../pages/content.vue'
// 引入子路由
import Frame from '../frame/subroute.vue'
// 引入子页面
import userIndex from '../pages/user/index.vue'
import userInfo from '../pages/user/info.vue'
import userLove from '../pages/user/love.vue'
Vue.use(Router)
// 配置路由
export default{
	mode: 'history',  // 去掉路由地址的#
	routes:[

  {
    path: '/',
    component: index
  },
  {
    path: '/content',
    component: content
  },
  {
  path: '/user',
  component: Frame,
  //component: resolve => require(['./page/linkParamsQuestion.vue'], resolve) 懒加载 访问某个页面的时候才加载资源
  children: [
    {path: '/',component: userIndex},
    {path: 'info',component: userInfo},
    {path: 'love',component: userLove}
  ],
	},
	{
	  path: '**',   // 错误路由[写在最后一个]
	  redirect: '/'   //重定向
	}
]
}
